<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Enquiry;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $limiteds = [];
        return view('home.index', compact('limiteds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'full_name'    => 'required',
            'email'          => 'required|email',
            'phone'  => 'required',
        ];

        $messages = [
            'full_name.required'       => 'Silahkan lengkapi nama lengkap Anda',
            'email.required'             => 'Silahkan lengkapi email Anda',
            'phone.required'             => 'Silahkan lengkapi no telepon/hp Anda',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $enquiry = Enquiry::create([
            'nama'            => $request->input('full_name'),
            'email'           => $request->input('email'),
            'telephone'       => $request->input('phone'),
            'created_date'    => now(),
            'is_read'         => 1,
        ]);
        $enquiry->save();
        return redirect('thankyou/'.$enquiry->id)->with('success', 'Event has been successfuly booked');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function thankyou(Request $request, $id)
    {
        $enquiry = Enquiry::findOrFail($id);
        if(!session('success')){
            abort(404);
        }
        return view('home.thankyou', compact('enquiry'));
    }
}
