<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') &mdash; {{ config('app.name') }}</title>

    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <link rel="canonical" href="{{ url('/') }}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title"
        content="@yield('title') - {{ config('app.name') }}" />
    <meta property="og:description" content="" />
    <meta property="og:url" content="{{ config('app.url') }}" />
    <meta property="og:site_name"
        content="{{ config('app.name') }}" />

    <link rel="shortcut icon" href="{{ url('/assets') }}/img/favicon/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/assets') }}/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/assets') }}/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/assets') }}/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="{{ url('/assets') }}/img/favicon/site.webmanifest">

    <link
      rel="stylesheet"
      href="{{ url('/assets') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css"
    />
    <link href="{{ url('/assets') }}/css/tailwind.min.css" rel="stylesheet">
    <link href="{{ url('/assets') }}/font/fonts.css" rel="stylesheet">
    <link href="{{ url('/assets') }}/css/customs.css" rel="stylesheet">

    @stack('styles')

    {{-- Scripts --}}
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="text-gray-800 antialiased">
  <main>
    @yield('content')
  </main>
  @include('layouts.footer')
    
    @stack('scripts')
</body>

</html>
