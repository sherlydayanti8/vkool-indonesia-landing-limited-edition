@extends('layouts.app')

@section('title', "Home")

@push('styles')
@endpush

@section('content')
<section class="bg-red-200 text-green-900 relative">

  <div class="min-h-screen hero-image bg-right-bottom bg-cover flex" style="background-image: url({{ url('/assets') }}/img/background-vkool-landing-form.jpg);">
    <div class="relative container mx-auto p-4 flex items-center  justify-center z-10">
      <div class="flex flex-wrap justify-center">
        <div class="w-full lg:w-12/12 px-4">
          <div class="relative flex flex-col min-w-0 break-words w-full mb-10 shadow-lg rounded-lg bg-white text-center">
            <div class="flex-auto p-5 lg:p-10 items-center  justify-center ">

                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 30" stroke="currentColor" stroke-width="1" fill-rule="evenodd" clip-rule="evenodd" class="w-full h-48">
                <path d="M12 0c6.623 0 12 5.377 12 12s-5.377 12-12 12-12-5.377-12-12 5.377-12 12-12zm0 1c6.071 0 11 4.929 11 11s-4.929 11-11 11-11-4.929-11-11 4.929-11 11-11zm7 7.457l-9.005 9.565-4.995-5.865.761-.649 4.271 5.016 8.24-8.752.728.685z"/>
                </svg>

              <h4 class="text-2xl font-semibold">Terimakasih, Permintaan Anda berhasil terkirim.</h4>
              <p class="leading-relaxed mt-1 mb-4 text-gray-600">
                Untuk proses lebih lanjut, team kami akan segera menghubungi Anda.
              </p>

              <div class="text-center mt-6 pt-5">
                <a
                  class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                  href="{{url('/')}}" style="transition: all 0.15s ease 0s;">
                  Kembali ke Home
                </a>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@push('scripts')
  @include('home.index-scripts')
@endpush
