@extends('layouts.app')

@section('title', "Home")

@push('styles')
@endpush

@section('content')
<div class="relative pt-5 pb-5 md:pb-16 lg:pb-16 flex content-center items-center justify-center"
  style="min-height: 75vh;">
  <div class="absolute top-0 w-full h-full bg-center bg-cover"
    style='background-image: url("{{ url('/assets') }}/img/background-vkool-landing-top.jpg");'>
  </div>
  <div class="container relative mx-auto pt-100">
    <div class="items-center flex flex-wrap">
      <div class="w-full w-12/12 lg:w-6/12 md:w-6/12 px-4 ml-auto mr-auto pb-10 md:order-last">
        <div class='ResponsiveYTPlayer'><iframe class="youtube-player inset-0 w-full h-full rounded-lg" id="player"
            type="text/html"
            src="https://www.youtube.com/embed/cvem6Qq2fC8?wmode=opaque&autohide=1&autoplay=1&enablejsapi=1&loop=1&controls=0&showinfo=0&autohide=1&modestbranding=1&playlist=cvem6Qq2fC8"
            frameborder="0" muted="muted"></iframe></div>
      </div>
      <div class="w-full w-12/12 lg:w-6/12 md:w-6/12 px-4 ml-auto mr-auto pb-10">
        <div class="grid justify-items-stretch pb-10">
          <img class="justify-self-center w-8/12 lg:w-6/12 md:w-6/12"
            src="{{ url('/assets') }}/img/vkool-limited-edition-logo.png" alt="">
        </div>
        <p class="font-helvetica font-semibold text-3xl pb-5 text-center md:text-left lg:text-left">
          Luxury Comfort. Legendary Performance
        </p>
        <p class="font-helvetica mt-4 text-lg text-justify">
          V-KOOL Limited Edition adalah kaca film terbaik dan tercanggih yang pernah ada,
          kami persembahkan bagi Anda individu terpilih.
          V-KOOL Limited Edition mampu menolak 98% panas matahari dan mempunyai Total Solar Energy Heat Rejection
          (TSER)
          sebesar 74%, memberikan Anda performa tertinggi kaca film dan privasi maksimal yang Anda inginkan.
        </p>
      </div>
    </div>
  </div>
</div>


<div class="relative pb-32 flex content-center items-center justify-center min-height-img">
  <div class="absolute top-0 w-full h-full bg-center bg-cover visible md:invisible "
    style='background-image: url("{{ url('/assets') }}/img/mo-background-vkool-landing-second.jpg");'>
  </div>
  <div class="absolute top-0 w-full h-full bg-center bg-cover invisible md:visible "
    style='background-image: url("{{ url('/assets') }}/img/background-vkool-landing-second.jpg");'>
  </div>
</div>


<div class="relative pt-16 pb-32 flex content-center items-center justify-center min-height-img-car">
  <div class="absolute top-0 w-full h-full bg-center bg-cover visible md:invisible"
    style='background-image: url("{{ url('/assets') }}/img/mo-background-vkool-landing-car.jpg");'>
  </div>
  <div class="absolute top-0 w-full h-full bg-center bg-cover invisible  md:visible"
    style='background-image: url("{{ url('/assets') }}/img/background-vkool-landing-car.jpg");'>
  </div>
</div>


<section class="pb-20 bg-white 2xl:-mt-52 xl:-mt-36 md:-mt-28 sm:-mt-28 -mt-20">

  <div class="mx-auto">
    <div class="flex flex-wrap">
      <div class="w-full md:w-6/12 lg:w-6/12 sm:w-8/12 w-4/12">
        <div class="relative flex flex-col min-w-0 break-words w-full mb-8">
          <div class="flex-auto bg-transparent">
            <img class="object-full object-bottom w-full pb-10" src="{{ url('/assets') }}/img/vkool-product-package.png" alt="">
          </div>
        </div>
      </div>

      <div class="w-full lg:w-6/12 md:w-6/12 sm:w-12/12 px-4 ml-auto mr-auto pb-10 md:mt-64">
        <p class="font-helvetica font-semibold text-3xl pb-5 text-center md:text-left lg:text-left">
          Luxury Comfort. Legendary Performance
        </p>
        <p class="font-helvetica mt-4 text-lg text-justify">
          V-KOOL Limited Edition adalah kaca film terbaik dan tercanggih yang pernah ada,
          kami persembahkan bagi Anda individu terpilih.
          V-KOOL Limited Edition mampu menolak 98% panas matahari dan mempunyai Total Solar Energy Heat Rejection
          (TSER)
          sebesar 74%, memberikan Anda performa tertinggi kaca film dan privasi maksimal yang Anda inginkan.
        </p>
      </div>
    </div>
  </div>
</section>


<div class="relative pb-32 flex content-center z-10" style="min-height: 65vh;">
  <div class="absolute top-0 w-full h-full bg-center bg-cover"
    style='background-image: url("{{ url('/assets') }}/img/background-vkool-landing-card-black.jpg");'>
  </div>
  <div class="container relative mx-auto pt-16">
    <div class="items-center flex flex-wrap">

      <div class="w-full w-12/12 px-4 ml-auto mr-auto pb-10">
        <div class="grid justify-items-stretch pb-10">
          <img class="justify-self-center w-8/12 lg:w-2/12 md:w-6/12"
            src="{{ url('/assets') }}/img/vkool-limited-edition-logo-light.png" alt="">
        </div>
        <p class="font-helvetica font-semibold text-3xl pb-5 text-center text-white">
          Privileged Card
        </p>
        <p class="font-helvetica font-semibold text-3xl pb-5 text-center text-white">
          Dapatkan layanan prioritas yang eksklusif.
        </p>
      </div>

    </div>
  </div>
</div>


<div class="relative pb-10 flex content-center 2xl:-mt-80 xl:-mt-64 lg:-mt-52 md:-mt-48 -mt-40">
  <div class="absolute top-0 w-full h-full bg-center bg-cover"
    style='background-image: url("{{ url('/assets') }}/img/background-vkool-landing-card-gold.jpg");'>
  </div>
  <div class="container relative mx-auto pt-16 content-center">

    <div class="flex">
      <div class="flex-grow">
      </div>
      <div class="flex-shrink xl:w-6/12 lg:w-6/12 md:w-8/12 sm:w-12/12 z-10">
        <img class="object-full w-full pb-10" src="{{ url('/assets') }}/img/vkool-cards.png" alt="">
      </div>
      <div class="flex-grow">
      </div>
    </div>

    <div class="items-center flex flex-wrap">
      <div class="w-full lg:w-6/12 md:w-6/12 sm:w-6/12 px-4 ml-auto mr-auto pb-5">
        <p class="font-helvetica font-semibold text-3xl text-center text-white">
          Benefit:
        </p>
      </div>
    </div>
    <div class="items-center flex flex-wrap pb-15">
      <div class="w-full lg:w-6/12 md:w-8/12 sm:w-6/12 px-4 ml-auto mr-auto">
        <div class="items-center flex flex-wrap">
          <div class="w-full w-6/12 lg:w-6/12 md:w-6/12 px-5">
            <img class="object-none object-center lg:object-right xl:object-right md:object-right w-full h-36"
              src="{{ url('/assets') }}/img/vkool-benefit-limited-icon-1.png" alt="">
            <p class="font-helvetica font-semibold text-2xl text-center xl:text-right lg:text-right md:text-right">
              Potongan tambahan sebesar 5%
            </p>
          </div>
          <div class="w-full w-6/12 lg:w-6/12 md:w-6/12 px-5">
            <img class="object-none object-center lg:object-left xl:object-left md:object-left w-full h-36"
              src="{{ url('/assets') }}/img/vkool-benefit-limited-icon-2.png" alt="">
            <p class="font-helvetica font-semibold text-2xl text-center xl:text-left lg:text-left md:text-left">
              Free satu tahun premium car wash
            </p>
          </div>
        </div>
      </div>

      <div class="w-full w-12/12 px-4 ml-auto mr-auto pt-20">
        <p class="font-helvetica font-semibold">
          *syarat dan ketentuan berlaku
        </p>
      </div>
    </div>
  </div>
</div>

<div class="relative pb-20 flex content-center z-10" style="min-height: 40vh;">
  <div class="absolute top-0 w-full h-full bg-center bg-cover"
    style='background-image: url("{{ url('/assets') }}/img/background-vkool-landing-form.jpg");'>
  </div>

  @include('home.form')
  
  @if($errors->any())
  @include('home.message')
  @endif
</div>

@endsection

@push('scripts')
  @include('home.index-scripts')
@endpush
